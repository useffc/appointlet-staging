(function() {
  'use strict';

  angular.module('appointlet.www', [
    // 'appointlet',
    'restangular',
    // 'http-auth-interceptor',
    'ui.bootstrap.dropdown',
    'ui.bootstrap.tooltip',
    'ui.bootstrap.buttons',
  ])

  // configure REST adapter
  .config(function(RestangularProvider, settings) {
    RestangularProvider.setBaseUrl(settings.API_URL);
  });

  // // configure appointlet loader
  // .config(function(AppointletProvider, settings) {
  //   AppointletProvider.defaults.host = settings.SCHEDULER_HOST;
  // })

  // // configure authentication
  // .run(function(Auth, Session, $rootScope, LoginModal, QueryString) {
  //   // See if the URL contained an access token.
  //   var hashParams = QueryString.decode(window.location.hash.substr(1));

  //   // If it did, then store in the session
  //   if (hashParams.access_token) {
  //     Session.create(hashParams.access_token);
  //   }

  //   // if there is an active session, authenticate the user
  //   if (Session.accessToken) {
  //     Auth.authenticate(Session.accessToken);
  //   }

  //   // listen for login required events and prompt the user if necessary
  //   $rootScope.$on('event:auth-loginRequired', LoginModal.open);
  // });

})();
