module.exports = function(grunt) {
  "use strict";

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-angular-templates');
  grunt.loadNpmTasks('grunt-aws-s3');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-ng-annotate');
  grunt.loadNpmTasks('grunt-karma');
  grunt.loadNpmTasks('grunt-svg-sprite');
  grunt.loadNpmTasks('grunt-contrib-clean');

  var svgDark = function(shape, sprite, callback) {
    var paths = shape.dom.getElementsByTagName('path');
    for (var i = 0; i < paths.length; i++) {
      paths[i].setAttribute('fill', 'white');
    }
    callback(null);
  };

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    clean: ["tmp", "dist"],

    svg_sprite: {
      sm: {
        expand: true,
        cwd: 'src/svg',
        src: ['*.svg'],
        dest: 'tmp',
        options: {
          shape: {
            dimension: {
              maxWidth: 24,
              maxHeight: 24
            },
            dest: 'svg/sm',
          },
          mode: {
            defs: true,
          }
        }
      },
      smDark: {
        expand: true,
        cwd: 'src/svg',
        src: ['*.svg'],
        dest: 'tmp',
        options: {
          shape: {
            dimension: {
              maxWidth: 24,
              maxHeight: 24
            },
            dest: 'svg/sm/dark',
          },
          transform: ['svgo', {
            dark: svgDark
          }],
          mode: {
            defs: true,
          }
        }
      },
      lg: {
        expand: true,
        cwd: 'src/svg',
        src: ['*.svg'],
        dest: 'tmp',
        options: {
          shape: {
            dimension: {
              maxWidth: 32,
              maxHeight: 32
            },
            dest: 'svg/lg',
          },
          mode: {
            defs: true,
          }
        }
      },
      lgDark: {
        expand: true,
        cwd: 'src/svg',
        src: ['*.svg'],
        dest: 'tmp',
        options: {
          shape: {
            dimension: {
              maxWidth: 32,
              maxHeight: 32
            },
            dest: 'svg/lg/dark',
          },
          transform: ['svgo', {
            dark: svgDark
          }],
          mode: {
            defs: true,
          }
        }
      },
      sprite: {
        expand: true,
        cwd: 'tmp/svg',
        src: ['**/*.svg'],
        dest: 'dist',
        options: {
          shape: {
            id: {
              generator: function(name) {
                return name.replace('.svg', '').split('/').reverse().map(function(piece) {
                  return 'icon-' + piece;
                }).join('.');
              },
            },
          },
          mode: {
            css: {
              dimensions: "",
              sprite: 'icons.svg',
              prefix: ".%s",
              bust: false,
              render: {
                css: {
                  dest: 'icons.css'
                }
              }
            },
          }
        }
      },
    },

    copy: {
      images: {
        expand: true,
        cwd: 'src/img/',
        src: ["*", "**/*"],
        dest: 'dist/img/',
      },
      views: {
        expand: true,
        cwd: 'src/views/',
        src: ["*.html"],
        dest: 'dist/',
      }
    },

    less: {
      app: {
        options: {
          compress: true,
          sourceMap: true,
          sourceMapURL: 'style.css.map',
        },
        files: {
          'dist/css/style.css': 'dist/css/style.less'
        }
      }
    },

    concat: {
      less: {
        files: {
          'dist/css/style.less': [
            // all of our less files
            'src/**/*.less',

            // icons
            'dist/css/icons.css',
          ]
        }
      },

      libs: {
        files: {
          'dist/js/libs.js': [
            'bower_components/underscore/underscore.js',
            'bower_components/angular/angular.js',
            'bower_components/restangular/src/restangular.js',

            // bootstrap widget dependencides
            'bower_components/angular-bootstrap/src/position/position.js',
            'bower_components/angular-bootstrap/src/bindHtml/bindHtml.js',
            'bower_components/angular-bootstrap/src/transition/transition.js',

            // bootstrap tooltip
            'bower_components/angular-bootstrap/src/tooltip/tooltip.js',
            '<%= ngtemplates.uiTooltip.dest %>',

            // bootstrap modal
            'bower_components/angular-bootstrap/src/modal/modal.js',
            '<%= ngtemplates.uiModal.dest %>',

            // bootstrap dropdown
            'bower_components/angular-bootstrap/src/dropdown/dropdown.js',

            // bootstrap buttons
            'bower_components/angular-bootstrap/src/buttons/buttons.js',
          ]
        }
      }
    },

    uglify: {
      logic: {
        options: {
          sourceMap: true,
        },
        files: {
          'dist/js/logic.min.js': ['dist/js/logic.js'],
        }
      },
      templates: {
        options: {
          sourceMap: true,
        },
        files: {
          'dist/js/templates.min.js': ['<%= ngtemplates.app.dest %>'],
        }
      },
      libs: {
        options: {
          sourceMap: true,
        },
        files: {
          'dist/js/libs.min.js': ['dist/js/libs.js'],
        }
      },
    },

    watch: {
      js: {
        files: ['src/**/*.js'],
        tasks: ['processLogic'],
        options: {
          spawn: false
        }
      },
      views: {
        files: ['src/views/*.html'],
        tasks: ['copy:views'],
        options: {
          spawn: false
        }
      },
      templates: {
        files: ['<%= ngtemplates.app.src %>'],
        tasks: ['processTemplates'],
        options: {
          spawn: false
        }
      },
      less: {
        files: ['src/**/*.less'],
        tasks: ['processStyles'],
        options: {
          spawn: false
        }
      },
    },

    // convert html templates into angular injected templates
    ngtemplates: {
      app: {
        options: {
          module: 'appointlet.www',
        },
        src: 'src/**/*.tmpl',
        dest: 'dist/js/templates.js',
      },
      uiModal: {
        options: {
          module: 'ui.bootstrap.modal',
          url: function(file) {
            return file.split('/').slice(-3).join('/');
          }
        },
        src: 'bower_components/angular-bootstrap/template/modal/*.html',
        dest: 'tmp/angular-bootstrap/modal-templates.js',
      },
      uiTooltip: {
        options: {
          module: 'ui.bootstrap.tooltip',
          url: function(file) {
            return file.split('/').slice(-3).join('/');
          }
        },
        src: 'bower_components/angular-bootstrap/template/tooltip/*.html',
        dest: 'tmp/angular-bootstrap/tooltip-templates.js',
      },
    },

    // add dependency injection annotations
    ngAnnotate: {
      logic: {
        files: {
          'dist/js/logic.js': ['src/**/*.js', '!src/**/*.spec.js']
        }
      }
    },

    // copy beta and production files to s3
    aws_s3: {
      prod: {
        options: {
          uploadConcurrency: 10,
          bucket: 'appointlet-production',
          access: 'public-read',
          accessKeyId: process.env.DEPLOY_AWS_ACCESS_KEY_ID,
          secretAccessKey: process.env.DEPLOY_AWS_SECRET_ACCESS_KEY,
        },

        files: [{
          expand: true,
          src: '**',
          cwd: 'dist/',
          dest: 'www/',
        }]
      },

      beta: {
        options: {
          uploadConcurrency: 10,
          bucket: 'appointlet-beta',
          access: 'public-read',
          accessKeyId: process.env.DEPLOY_AWS_ACCESS_KEY_ID,
          secretAccessKey: process.env.DEPLOY_AWS_SECRET_ACCESS_KEY,
        },

        files: [{
          expand: true,
          src: '**',
          cwd: 'dist/',
          dest: 'www/',
        }]
      }
    },


    // serve up files in dev
    connect: {
      server: {
        options: {
          base: 'dist',
          port: '9001',
        }
      }
    },

    // test runner
    karma: {
      unit: {
        configFile: 'karma.conf.js',
      }
    },
  });

  // builds vendor scripts
  grunt.registerTask('processLibs', [
    'concat:libs',
    'uglify:libs',
  ]);

  // builds our scripts
  grunt.registerTask('processLogic', [
    'ngAnnotate:logic',
    'uglify:logic',
  ]);

  // builds our templates
  grunt.registerTask('processTemplates', [
    'ngtemplates:app',
    'ngtemplates:uiModal',
    'ngtemplates:uiTooltip',
    'uglify:templates',
  ]);

  // builds our styles
  grunt.registerTask('processStyles', [
    'concat:less',
    'less',
  ]);

  // performs all builds
  grunt.registerTask('build', [
    'clean',
    'copy',
    'svg_sprite',
    'processStyles',
    'processTemplates',
    'processLogic',
    'processLibs',
  ]);

  // testing
  grunt.registerTask('test', ['karma']);

  // development
  grunt.registerTask('default', ['build', /*'test',*/ 'connect', 'watch']);

  // beta
  grunt.registerTask('beta', ['build', 'aws_s3:beta']);

  // production
  grunt.registerTask('prod', ['build', 'aws_s3:prod']);
};
