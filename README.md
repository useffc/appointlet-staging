### Preparing the environment ###

This assumes you have git/node/npm installed.

First clone the repo down and `cd` into it:
`$ git clone git@bitbucket.org:appointlet/www.git && cd www`

Install dependencies:
`$ npm install`

Install grunt-cli:
`$ npm install -g grunt-cli`

Build project and start local dev server:
`$ grunt`

You should now be able to open a browser and load http://127.0.0.1:9001/front.html

### HTML ###

The HTML for the pages are in `src/views`.  In production I will be using template inheritance, but for now each one specifies all html, head, body tags.  All views are pulling in the necessary JS/LESS, so you shouldn't have to tweak much there.

### LESS ###

The main LESS files are in `src/less` and include some notes inside of them on what they do.

One critical thing to note about them is that you don't need to write imports in your LESS files because grunt is setup to concatenate them all before rendering them into CSS.  I prefer this strategy for importing over the built-in LESS style because files don't need to concern themselves to the relative positions of others.

As for styles that are specific to a page, just drop them into the LESS file of the same name in `src/views`.  Again, don't worry about imports here.  Bootstrap variables and mixins will magically be available.

### Javascript ###

You shouldn't have to worry about Javascript at all.  I'm going to be watching this repo closely and will add in the interactions as you go along.